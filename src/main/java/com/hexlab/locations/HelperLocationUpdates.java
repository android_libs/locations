package com.hexlab.locations;

import android.app.Activity;
import android.content.Context;
import android.content.IntentSender;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;

/**
 * Created by oreste on 24/09/15.
 *
 * Class which contains easy methods in order to implement locations updates
 *
 */
public class HelperLocationUpdates implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, LocationListener {

    private final int REQUEST_RESOLVE_ERROR = 100;
    private GoogleApiClient mGoogleApiClient;
    private boolean mResolvingError=false,wasConnectedPreviously=false;
    private long interval, fastestInterval;
    private int accuracy;
    private Activity activity;
    private LocationUpdatesListener locationUpdatesListener;

    /**
     * fired when received a new location or an error is occurred
     */
    public interface LocationUpdatesListener{
        void onLocationChanged(Location location);
        void onError();
        void onConnectionSuspended();
    }

    /**
     *
     * @param context
     * @return true if the location manager is enabled, false otherwise
     */
    public static boolean isLocationManagerEnabled(Context context){
        LocationManager manager = (LocationManager)context.getSystemService(Context.LOCATION_SERVICE);
        return manager.isProviderEnabled(LocationManager.GPS_PROVIDER) || manager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
    }

    public HelperLocationUpdates(Context context, long interval, long fastestInterval, int accuracy, LocationUpdatesListener locationUpdatesListener) {
        connect(context, interval, fastestInterval, accuracy, locationUpdatesListener);
    }

    public HelperLocationUpdates(Activity activity, long interval, long fastestInterval, int accuracy, LocationUpdatesListener locationUpdatesListener){
        this.activity = activity;
        connect(activity, interval, fastestInterval, accuracy, locationUpdatesListener);
    }

    @Override
    public void onConnected(Bundle bundle) {
        wasConnectedPreviously = true;
        mResolvingError = false;
        getLocationUpdates();
    }

    @Override
    public void onConnectionSuspended(int i) {
        mResolvingError = false;
        if(locationUpdatesListener==null) return;
        locationUpdatesListener.onConnectionSuspended();
    }

    @Override
    public void onLocationChanged(Location location) {
        if(locationUpdatesListener==null) return;
        locationUpdatesListener.onLocationChanged(location);
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        if(locationUpdatesListener!=null) locationUpdatesListener.onError();

        if(activity==null) return;

        if (mResolvingError) return;

        if (connectionResult.hasResolution()) {
            try {
                mResolvingError = true;
                connectionResult.startResolutionForResult(activity, REQUEST_RESOLVE_ERROR);
            } catch (IntentSender.SendIntentException e) {
                // There was an error with the resolution intent. Try again.
                mGoogleApiClient.connect();
            }
        } else {
            // Show dialog using GooglePlayServicesUtil.getErrorDialog()
            GooglePlayServicesUtil.getErrorDialog(connectionResult.getErrorCode(), activity, REQUEST_RESOLVE_ERROR).show();
            mResolvingError = true;
        }
    }

    /**
     * connects to GoogleApiClient
     *
     * @param context
     * @param interval
     * @param fastestInterval
     * @param accuracy
     * @param locationUpdatesListener
     */
    public void connect(Context context, long interval, long fastestInterval, int accuracy, LocationUpdatesListener locationUpdatesListener){
        this.interval = interval; this.fastestInterval = fastestInterval; this.accuracy = accuracy; this.locationUpdatesListener = locationUpdatesListener;

        mGoogleApiClient = new GoogleApiClient.Builder(context)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        mGoogleApiClient.connect();
    }

    /**
     * reconnects to GoogleAPiClient
     */
    public void reconnect(){
        if(!wasConnectedPreviously){
            mGoogleApiClient.connect();
            return;
        }
        mGoogleApiClient.reconnect();
    }

    /**
     * destroys the objects created
     */
    public void destroy(){
        if(mGoogleApiClient!=null){
            removeLocationUpdates();
            mGoogleApiClient.unregisterConnectionCallbacks(this);
            mGoogleApiClient.unregisterConnectionFailedListener(this);
            locationUpdatesListener = null;
            mGoogleApiClient = null;
            wasConnectedPreviously = false;
        }
    }

    /**
     * removes the location update request
     */
    public void removeLocationUpdates(){
        if(mGoogleApiClient!=null && mGoogleApiClient.isConnected()){
            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
            mGoogleApiClient.disconnect();
        }
    }

    /**
     * initiates a new location update request
     */
    private void getLocationUpdates(){
        LocationRequest mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(interval);
        mLocationRequest.setFastestInterval(fastestInterval);
        mLocationRequest.setPriority(accuracy);

        LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
    }
}
